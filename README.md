# Reckon coding test
_by Khaled Bin A Quadir_

## Running the tests
```
npm install
npm test
```

## Running the server
```
npm install
npm start
```

_*Notes*_:
- Server will start on http://localhost:9999/
- Browsing the root url `/` will perform the task1 and render output as text to the browser.
- Browsing `/task2` will perform task2 and submit the desired json data to the given url. In addition, it will dump the data on the browser for verification.