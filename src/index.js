const server = require('http').createServer();
const config = require('./config.json');
const {task1} = require('./task1');
const {task2} = require('./task2');

const handleTask1 = async (res) => {
    const {boundsUrl, divisorUrl} = config.task1;
    const result = await task1(boundsUrl, divisorUrl);
    const output = Object.keys(result)
        .map(key => `${key}: ${result[key]}`)
        .join('\n');

    res.writeHead(200, {'content-type': 'text/plain'});
    res.end(output);
};

const handleTask2 = async (res) => {
    const {textToSearchUrl, subtextUrl, postUrl} = config.task2;
    const result = await task2(textToSearchUrl, subtextUrl, postUrl);

    res.writeHead(200, {'content-type': 'text/plain'});
    res.end(`Submitted the following:\n${JSON.stringify(result,null,4)}`);
};

server.on('request', async (req, res) => {

    switch (req.url) {
        case '/':
            await handleTask1(res);
            break;
        case '/task2':
            await handleTask2(res);
            break;
        default:
            res.writeHead(404);
            res.end();
    }
});

server.on('error', (e) => console.log('Error: ', e));

server.listen(config.port);
console.log(`Server running on http://localhost:${config.port}`);
console.log(`Go to http://localhost:${config.port}/task2 to test the task2`)