const {range, getJson, retryAsync} = require('./utils');

const getStrFromNumber = (num, divisors) =>
    divisors
        .map(obj => num % obj.divisor === 0 && num !== 0 ? obj.output : '')
        .join('');

const getStrForRange = (low, high, divisors) =>
    range(low, high)
        .reduce((p, c) => Object.assign(p, {[`${c}`]: getStrFromNumber(c, divisors)}), {});

const task1 = async (numberBoundsUrl, divisorInfoUrl) => {
    const {lower, upper} = await retryAsync(async () => getJson(numberBoundsUrl));
    const {outputDetails} = await retryAsync(async () => getJson(divisorInfoUrl));

    return getStrForRange(lower, upper, outputDetails);
};

module.exports = {getStrFromNumber, getStrForRange, task1};