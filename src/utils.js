const axios = require('axios');

const range = (low, high) =>
    [... Array(high - low + 1).keys()].map(k => k + low);

const getJson = async (url) =>
    (await axios.get(url)).data;

const postJson = async (url, data) =>
    await axios.post(url, data);

const retryAsync = async (func) => {
    while (true) {
        try {
            return await func();
        }
        catch (err) {
            // do nothing
        }
    }
};

module.exports = {getJson, postJson, retryAsync, range};