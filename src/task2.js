const {getJson, postJson, retryAsync} = require('./utils');

const findIndex = (text, sub) => {
    const textArr = text.toLowerCase().split('');
    const subArr = sub.toLowerCase().split('');

    const search = textArr
        .reduce((a, c, i) => {
            if (subArr[0] === c) {
                const match = subArr
                    .filter((s, j) => s === textArr[i + j]);
                if (match.length === subArr.length) a.push(i + 1)
            }
            return a;
        }, []);

    if (search.length === 0) return "<No Output>";
    return search.join(', ');
};

const task2 = async (textUrl, subTextUrl, postUrl) => {
    const {text} = await retryAsync(async () => getJson(textUrl));
    const {subTexts} = await retryAsync(async () => getJson(subTextUrl));

    const matches = subTexts
        .map(st => ({subtext: st, result: findIndex(text, st)}));

    const submission = {
        candidate: 'Khaled Bin A Quadir',
        text,
        results: matches
    };

    await retryAsync(async () => postJson(postUrl, submission));
    return submission;
};

module.exports = {findIndex, task2};