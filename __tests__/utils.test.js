const {retryAsync, range} = require('../src/utils');

describe('retryAsync(fn)', () => {
    it('should return immediately when no error encounterd', async () => {
        const func = jest.fn().mockReturnValue('DATA');

        const response = await retryAsync(func);

        expect(response).toBe('DATA');
        expect(func.mock.calls).toHaveLength(1);
    });

    it('should retry untill it gets a result', async () => {
        let i = 0;
        const fn = async () => {
            if (i < 3) {
                i++;
                throw Error('Something went wrong');
            }
            return 'DATA';
        }

        const response = await retryAsync(fn);

        expect(response).toBe('DATA');
    });
});

describe('range()', () => {
    it('should generate proper range', () => {
        const expected = [3, 4, 5, 6];
        const actual = range(3, 6);
        expect(actual).toEqual(expected);
    });
});