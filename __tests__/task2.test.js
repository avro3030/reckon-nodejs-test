const {findIndex} = require('../src/task2');

describe('findIndex', () => {
    it('should return <No Output> for empty text', () => {
        const actual = findIndex('', 'hello world');
        expect(actual).toEqual('<No Output>');
    });

    it('should return <No Output> for empty subtext', () => {
        const actual = findIndex('hello world', '');
        expect(actual).toEqual('<No Output>');
    });

    it('should return <No Output> for empty parameters', () => {
        const actual = findIndex('', '');
        expect(actual).toEqual('<No Output>');
    });

    it('should return position for exact match', () => {
        const actual = findIndex('hello', 'hello');
        expect(actual).toEqual('1');
    });

    it('should be case insensitive', () => {
        const actual = findIndex('hello', 'HeLlo');
        expect(actual).toEqual('1');
    });

    it('should return positions for multiple matches', () => {
        const actual = findIndex('hello, elly!', 'el');
        expect(actual).toEqual('2, 8');
    });
});