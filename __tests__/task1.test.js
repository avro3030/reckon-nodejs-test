const {getStrFromNumber, getStrForRange} = require('../src/task1');

describe('getStrFromNumber()', () => {
    it('should return empty string for no divisor', () => {
        const actual = getStrFromNumber(42, []);
        expect(actual).toBe('');
    });

    it('should return empty string for 0', () => {
        const actual = getStrFromNumber(0, [{divisor: 2, output: 'Fizz'}]);
        expect(actual).toEqual('');
    });

    it('should return string represention for a divisor', () => {
        const expected = 'Fizz';
        const actual = getStrFromNumber(
            6,
            [{divisor: 3, output: 'Fizz'}]
        );

        expect(actual).toEqual(expected);
    });

    it('should return empty string for no match', () => {
        const expected = '';
        const actual = getStrFromNumber(
            13,
            [{divisor: 3, output: 'Fizz'}, {divisor: 5, output: 'Buzz'}]
        );

        expect(actual).toBe(expected);
    });

    it('should return combined string for multiple matches', () => {
        const expected = 'FizzBuzz';
        const actual = getStrFromNumber(
            15,
            [{divisor: 3, output: 'Fizz'}, {divisor: 5, output: 'Buzz'}]
        );

        expect(actual).toBe(expected);
    });
});

describe('getStrForRange', () => {
    it('should return all the keys', () => {
        const low = 3;
        const high = 6;
        const expectedKeys = ['3', '4', '5', '6'];

        const result = getStrForRange(low, high, []);
        const actualKeys = Object.keys(result);

        expect(actualKeys).toEqual(expectedKeys);
    });

    it('should contain empty values for no divisor match', () => {
        const result = getStrForRange(1, 7, [{divisor: 2, output: 'Fizz'}]);
        expect(result).toHaveProperty('3', '');
    });

    it('should contain proper value for divisor matches', () => {
        const divisors = [
            {divisor: 2, output: 'Fizz'},
            {divisor: 3, output: 'Buzz'}
        ]

        const result = getStrForRange(1, 7, divisors);

        expect(result).toHaveProperty('2', 'Fizz');
        expect(result).toHaveProperty('3', 'Buzz');
        expect(result).toHaveProperty('6', 'FizzBuzz');
    });
});